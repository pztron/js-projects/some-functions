var fs = require('fs')
var input = require('prompt-sync')()

try {
    var lipsum = fs.readFileSync('lipsum.txt', 'utf8')
    console.log(lipsum)    
} catch(e) {
    console.log('Error:', e.stack)
}

const twoArrayAverage = (theArray) => {
    
    let total = 0
    for ( var x = 0; x < theArray.length; x++) { 
        if ( theArray[x] == "-1") { 
            theArray.splice(x, 1); x--
        }
    } 
    for (i in theArray) {
        total += theArray[i]
    }
    return total / theArray.length
}

const fourIndexes = (arr, val) => {
    let indexes = []
    for (let i in arr) {
        if (arr[i] == val) {
            indexes.push(i)
        }
    }
    return indexes
}

const sentencify = (lipsum) => {
    let sentences = lipsum.split(". ")
    return sentences
}

const wordify = (lipsum) => {
    let words = lipsum.split(" ")
    return words
}

const cleanWordify = (lipsum) => {
    let words = lipsum.split(" ")
    let regex = /[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/g
    for (let i in words) {
        words[i] = words[i].replace(regex, '').toLowerCase()
    }
    return words
}

const getCharsBefore = (str, chr) =>  {
    var index = str.lastIndexOf(chr)
    if (index != -1) {
        return(str.substring(0, index) + "x")
    }
    return ("")
}

const fiveMultiplyArray = (arr) => {
    let total = 1
    for (let i in arr) {
        if (arr[i].includes('/')) {
            total = total * (1 / parseFloat(arr[i].split("/")[1], 10))
        } else {
            total = total*arr[i] 
        }
       
    }
    return total
}

const questionOne = () => {
    for (let i in sentencify(lipsum)) {
        console.log(sentencify(lipsum)[i] + " contains " + wordify(sentencify(lipsum)[i]).length + " words.")
        
    }
}

const fourMode = (arr) => {
    let obj = {}, count = 0, which = []
    
    arr.forEach(ea => {
        if (!obj[ea]) {
            obj[ea] = 1
        } else {
            obj[ea]++
        }
    
        if (obj[ea] > count) {
            count = obj[ea];
            which = [ea]
        } else if (obj[ea] === count) {
            which.push(ea)
        }
    })
    
    return which
}
questionOne()

const alphabet = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

const averagewordPos = () => {
    for (let i in alphabet) {
        let letterFound = []
        for (let x in sentencify(lipsum)) {
            let sentenceX = sentencify(lipsum)[x].toLowerCase()
            letterFound.push(sentenceX.indexOf(alphabet[i])) 
            if (x == 44) {
                console.log(alphabet[i] + " found on average at position " + twoArrayAverage(letterFound))
            }
        }

    }
}

averagewordPos()

const letterCount = () => {
    for (let i in alphabet) {
        let letterCount = []
        for (let x in wordify(lipsum)) {
            let wordX = wordify(lipsum)[x].toLowerCase()
            letterCount.push((wordX.split(alphabet[i]).length - 1))

            if (x == 344) {
                if (letterCount.indexOf(Math.max(...letterCount)) == 0) 
                    console.log(alphabet[i] + " is not found")
                else {
                    console.log(wordify(lipsum)[letterCount.indexOf(Math.max(...letterCount))] + " has the most of " + alphabet[i])
                }
            }

        }

    }

}

letterCount()

const wordBefore = () => {
    for (let i in cleanWordify(lipsum)) {
        let wordBefore = []
        for ( let x in fourIndexes( cleanWordify(lipsum), cleanWordify(lipsum)[i].toLowerCase() ) ) {
            wordBefore.push(cleanWordify(lipsum)[(fourIndexes(cleanWordify(lipsum), cleanWordify(lipsum)[i].toLowerCase())[x] - 1)] )
            if (x == fourIndexes( cleanWordify(lipsum), cleanWordify(lipsum)[i].toLowerCase() ).length - 1) {
                console.log("For " + cleanWordify(lipsum)[i] + " the most common word(s) before it was/were:")
                console.log(fourMode(wordBefore))
            }
        }
    }
}

wordBefore()
// takes input in the form of "3^10" or "5^-10"
const largeRecursive = (largeExponent) => {
    if (largeExponent.split("^")[1] === "2" || largeExponent.split("^")[1] === "-2") { 
        if (largeExponent.split("^")[1].includes("-")) {
            largeExponent = getCharsBefore(largeExponent, "x") + "1/" + (largeExponent.split("^")[0].split("x").pop() ** 2)
            console.log(largeExponent)
            console.log(fiveMultiplyArray(largeExponent.split("x")))
            console.log("Complete")
        } else {
            largeExponent = getCharsBefore(largeExponent, "x") + largeExponent.split("^")[0].split("x").pop() ** 2
            console.log(largeExponent)
            console.log(fiveMultiplyArray(largeExponent.split("x")))
            console.log("Complete")
        }
    } else if (largeExponent.split("^")[1] % 2 == 0 || -0) {
        largeExponent = getCharsBefore(largeExponent, "x") + largeExponent.split("^")[0].split("x").pop() ** 2 + "^" + largeExponent.split("^")[1] / 2
        console.log(largeExponent)
        questionFive(largeExponent)
    } else {
        base = largeExponent.split("^")[0].split("x").pop()
        exponent = largeExponent.split("^")[1]
        largeExponent = getCharsBefore(largeExponent, "x") + base + "x" + base + "^" + (exponent -1)
        console.log(largeExponent)
        questionFive(largeExponent)
    }
}

largeRecursive(input('Large exponent in format "3^10: '))


const largeInterate = (largeExponent) => {
    while (true) {
        if (largeExponent.split("^")[1] % 2 == 0 || -0) {
            largeExponent = getCharsBefore(largeExponent, "x") + largeExponent.split("^")[0].split("x").pop() ** 2 + "^" + largeExponent.split("^")[1] / 2
            console.log(largeExponent)
            if (largeExponent.split("^")[1] === "2" || largeExponent.split("^")[1] === "-2") { 
                if (largeExponent.split("^")[1].includes("-")) {
                    largeExponent = getCharsBefore(largeExponent, "x") + "1/" + (largeExponent.split("^")[0].split("x").pop() ** 2)
                    console.log(largeExponent)
                    console.log(fiveMultiplyArray(largeExponent.split("x")))
                    console.log("Complete")
                    process.exit(0)
                } else {
                    largeExponent = getCharsBefore(largeExponent, "x") + largeExponent.split("^")[0].split("x").pop() ** 2
                    console.log(largeExponent)
                    console.log(fiveMultiplyArray(largeExponent.split("x")))
                    console.log("Complete")
                    process.exit(0)
                }
            }
        } else {
            base = largeExponent.split("^")[0].split("x").pop()
            exponent = largeExponent.split("^")[1]
            largeExponent = getCharsBefore(largeExponent, "x") + base + "x" + base + "^" + (exponent -1)
            console.log(largeExponent)
            if (largeExponent.split("^")[1] === "2" || largeExponent.split("^")[1] === "-2") { 
                if (largeExponent.split("^")[1].includes("-")) {
                    largeExponent = getCharsBefore(largeExponent, "x") + "1/" + (largeExponent.split("^")[0].split("x").pop() ** 2)
                    console.log(largeExponent)
                    console.log(fiveMultiplyArray(largeExponent.split("x")))
                    console.log("Complete")
                    process.exit(0)
                } else {
                    largeExponent = getCharsBefore(largeExponent, "x") + largeExponent.split("^")[0].split("x").pop() ** 2
                    console.log(largeExponent)
                    console.log(fiveMultiplyArray(largeExponent.split("x")))
                    console.log("Complete")
                    process.exit(0)
                }
            }
        }
    }
    
}

largeInterate(input('Large exponent in format "3^10: '))